import { Injectable, EventEmitter} from '@angular/core';
import * as signalR from "@aspnet/signalr";
import { SignalViewModel } from "../models/signal-view-model";

@Injectable({
  providedIn: 'root'
})
export class SignalRService {

  private hubConnection!: signalR.HubConnection;
  private hubConnectionOfNotification!: signalR.HubConnection;
  signalReceived = new EventEmitter<SignalViewModel>();

  constructor() {
    this.buildConnection();
    this.startConnection();
  }

  private buildConnection = () => {
    this.hubConnection = new signalR.HubConnectionBuilder()
      .withUrl("http://localhost:62827/signalHub") //use your api adress here and make sure you use right hub name.
      .build();
      
      this.hubConnectionOfNotification = new signalR.HubConnectionBuilder()
      .withUrl("http://localhost:62827/RealTimeNotificationHub") //use your api adress here and make sure you use right hub name.
      .build();
  };

  private startConnection = () => {
    this.hubConnection
      .start()
      .then(() => {
        console.log("Connection Started...");
        this.registerSignalEvents();
      })
      .catch(err => {
        console.log("Error while starting connection: " + err);

        //if you get error try to start connection again after 3 seconds.
        setTimeout(() => {
          this.startConnection();
        }, 3000);
      });


      this.hubConnectionOfNotification
      .start()
      .then(() => {
        this.hubConnectionOfNotification.invoke("SendRealTimeNotifications").catch(err => console.error(err));
        console.log("Connection Started... for Real Time Notifications.....");
        this.registerRealTimeNotificationEvents();
      })
      .catch(err => {
        console.log("Error while starting connection of RealTimeNotifications: " + err);

        //if you get error try to start connection again after 3 seconds.
        setTimeout(() => {
          this.startConnection();
        }, 3000);
      });

  };

  private registerSignalEvents() {
    this.hubConnection.on("SignalMessageReceived", (data: SignalViewModel) => {
      //debugger;
      this.signalReceived.emit(data);
    });
  }

  private registerRealTimeNotificationEvents() {
    this.hubConnectionOfNotification.on("RecieveNotification", (data: SignalViewModel) => {
      //debugger;
      //this.signalReceived.emit(data);
      console.log(data);
        alert("I'm notified");
    });
  }
  
}
