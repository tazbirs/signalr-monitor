﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SignalrDemoAPI.Models
{
    public struct SignalViewModel
    {
        public string EmployeeId { get; set; }
        public string EmployeeName { get; set; }
        public string Description { get; set; }
        public string SignalStamp { get; set; }
    }
}
