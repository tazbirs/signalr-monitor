﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SignalrDemoAPI.Models
{

    public struct SignalInputModel
    {
        public string Description { get; set; }
        public string EmployeeName { get; set; }
        public string EmployeeId { get; set; }
/*        public string Area { get; set; }
        public string Zone { get; set; }*/
    }
}
