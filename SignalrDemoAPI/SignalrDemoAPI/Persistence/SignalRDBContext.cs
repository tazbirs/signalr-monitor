﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SignalrDemoAPI.Persistence
{
    public class SignalRDBContext: DbContext
    {
        public SignalRDBContext(DbContextOptions<SignalRDBContext> options):base(options) { 

        }

        public DbSet<EmployeeDataModel> Employees { get; set; }
    }
}
