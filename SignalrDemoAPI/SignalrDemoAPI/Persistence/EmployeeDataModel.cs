﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace SignalrDemoAPI.Persistence
{
    public class EmployeeDataModel
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public string Description { get; set; }
        public string EmployeeName { get; set; }
        public string EmployeeId { get; set; }
        public DateTime SignalDate { get; set; }
    }
}
