﻿using Microsoft.AspNetCore.Mvc.ModelBinding;
using Microsoft.EntityFrameworkCore;
using SignalrDemoAPI.Models;
using SignalrDemoAPI.Persistence;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SignalrDemoAPI.Services
{
    public class SignalService : ISignalService
    {
        private readonly SignalRDBContext _signalRDBContext;

        public SignalService(SignalRDBContext signalRDBContext)
        {
            _signalRDBContext = signalRDBContext;
        }
        public async Task<bool> SaveSignalAsync(SignalInputModel inputModel)
        {
            try
            {
                //map input model to data model
                //at this point we assume a signal arrives only one time and it's unique
                EmployeeDataModel objEmployee = new EmployeeDataModel();
                objEmployee.EmployeeName = inputModel.EmployeeName;
                objEmployee.Description = inputModel.Description;
                objEmployee.EmployeeId = inputModel.EmployeeId;
                objEmployee.SignalDate = DateTime.Now;

                //execute some business rules according to your cases.

                //if you decide to save signal add it to the db context
                _signalRDBContext.Employees.Add(objEmployee);

                //save changes and if the signal has stored in db return true.
                return await _signalRDBContext.SaveChangesAsync() > 0;
            }
            catch (Exception exception)
            {
                //log the exception or take some actions

                return false;
            }
        }

        public async Task<List<SignalViewModel>> getAllEmployeeAsync()
        {
            try
            {
               var query = from employee in _signalRDBContext.Employees
                            select new SignalViewModel() { 
                            EmployeeId = employee.EmployeeId,
                            EmployeeName= employee.EmployeeName,
                            Description = employee.Description,
                                SignalStamp = employee.SignalDate.ToString()

                            } ;
                return await query.ToListAsync();
            }
            catch (Exception exception)
            {
                //log the exception or take some actions

                return null;
            }
        }
    }
}
