﻿using SignalrDemoAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SignalrDemoAPI.Services
{
    public interface ISignalService
    {
        Task<bool> SaveSignalAsync(SignalInputModel inputModel);
        Task<List<SignalViewModel>> getAllEmployeeAsync();
    }
}
